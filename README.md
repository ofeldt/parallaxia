[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/M4M64N5IK)

# DISCLAIMER
This module is in early development and may cause bugs that could affect your game. Please test it out thoroughly before you unleash it on your players. For quick help ping `@wonko` on the Foundry Discord.

The effect is cool and simple to achieve, but should be used with care. Players may very easily get motion sickness, and staring for hours at a repeating nauseating pattern ain't no fun in my book.

Depending on the images used, it may soemtimes be easier to render out to video and just have a looping video background. A simple video loop may be smaller than several huge PNGs with transparency, and may be more performant.

![Dragon Flight Example](doc/dragonflight.webm)

# Installation
Manifest : [https://gitlab.com/reichler/parallaxia/raw/master/parallaxia/module.json](https://gitlab.com/reichler/parallaxia/raw/master/parallaxia/module.json)

# Parallax with Tiling Textures
## Technical foundation
Instances of `TilingTile` are placed on a `CanvasLayer`, similar to a normal `PIXI.Sprite`. Motion is induced with time-varying offsets of `tilePosition`, see [PIXI.TilingSprite](https://pixijs.download/dev/docs/PIXI.TilingSprite.html).
The structure of the code was shamelessly taken from U~Man's [FXMaster](https://gitlab.com/mesfoliesludiques/foundryvtt-fxmaster/-/tree/master).

![Tiling textures](doc/tiling_texture.jpg)

## Testing it
Create a scene of sufficient size. Create a tile. With tile tools selected, open Tile HUD and select the "Make Parallaxia Tile" button.

![Creating a Tile](doc/tile_setup.webm)

## The Good
- I guess it kind of works?

## The Bad
- no texture swapping
- no timelining
- may require large PNGs for transparency -> huge files
- may look terrible/janky on lower fps

## The Wishlist
- alpha for tile
- multiple layers
- motion visualization in config
- config live update
- texture alpha based on scene darkness
- parallax/position based on scene zoom and view location for that extra depth effect

## Performance
- uses more GPU than static textures. On my 1060Ti GPU usage goes from 18% to 28% for three large moving textures.
- runs ok on a laptop with an Intel HD620. Due to PIXIs default renderer eagerly rebuilding the scene each frame, the GPU is locked even with a static scene, so there's no difference there. Chomps your battery like a hungry Gnoll.
- to make this work, the Haste module can't slow down rendering

# Texture preparation
In general, all one needs is a texture that tiles along the required axis.

Difficulty of making a non-tiling texture tiling depends strongly on the image and ones image editing skills. In general, the faster the motion the less perfect loop transitions have to be.

## Example
Let's use this image of the Lagoon Nebula taken by Hubble. Throw it in Photoshop (I assume GIMP can do similar).

Offset the image to see how the tile boundary looks along your axis of choice (Filter->Other->Offset). Then we "just" have to paint out the boundary.
There's plenty tutorials on how to make tiling textures. Here's the quick version. 

![Tiling in Photoshop](doc/PS_tiling.jpg)

## Notes
To avoid aliasing artifacts, a low-pass filter should be applied. Either Gaussian-Blur, especially to convey depth or Motion-Blur, works well to convey high speeds.

For elements not requiring transparency, use JPEG. Those can be compressed pretty hard (e.g. 35% quality) and even be lower resolution, saving space.

For images with transparency on the texture boundary sometimes a visible line appears. That can be fixed by offsetting the tilePosition by a pixel or two in that axis, avoiding the aliasing of the wraparound.



