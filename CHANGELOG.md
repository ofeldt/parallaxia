# 0.2.x
- initial values representing state on scene load
    - state prior to any custom transformation script modifications
    - closely tied in parameters like width, height, rotation etc. to non-parallaxia tile

- user control flags read by custom scripts to e.g. initiate transitions from macros, pause, etc.

