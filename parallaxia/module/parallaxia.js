import {ParallaxiaManager} from "./ParallaxiaManager.js";
import {paraldbg, parallog} from "./logging.js";
import {ParallaxiaTileState, defaultState} from "./ParallaxiaTile.js";
import {ParallaxiaTileConfig} from './ParallaxiaTileConfig.js';

CONFIG.debug.parallaxia = true;
CONFIG.ParallaxiaTile = {
    ParallaxiaTileState: ParallaxiaTileState,
    defaultState: defaultState,
    documentClass: TileDocument,
    layerClass: MapLayer,
    objectClass: Tile,
    sheetClass: ParallaxiaTileConfig
};

class ParallaxiaIcons {
    static async addParallaxiaButton(app, html, data) {
        let tiles = canvas.activeLayer.controlled;
        if (tiles === undefined || !tiles.length) return

        let isActive = tiles[0].isParallaxia ? " active" : "";
        let btn_div = $(`<div class="control-icon${isActive}" data-action="parallaxia"><img src="icons/svg/waterfall.svg" width="36" height="36" title="Make Parallaxia Tile" /></div>`);

        html.find('div.col.right').append(btn_div);
        btn_div.find('img').click(async (ev) => {
            tiles.forEach(tile => {
                paraldbg("Requested to make parallaxia:");
                paraldbg(tile);
                canvas.parallaxiaManager.makeParallaxia(tile)
                    .then(_ => {
                        canvas.parallaxiaManager.add_target(tile.data._id);
                        btn_div.addClass("active");
                        // tile.sheet.render(true);
                        // TODO: Somehow make the button active
                    });
            });
        });
    }
}

Hooks.once("init", async () => {
    // this should be done with libWrapper?
    TileDocument.prototype._getSheetClass = function () {
        paraldbg("Sheet lookup for tile ...");
        paraldbg(this);
        if (this.data.flags?.parallaxia) {
            return ParallaxiaTileConfig;
        }
        const cfg = CONFIG[this.documentName];
        return cfg?.sheetClass || null;
    }
});


Hooks.once("canvasInit", async (canvas) => {
    parallog('Starting ParallaxiaManager');
    canvas.parallaxiaManager = new ParallaxiaManager();
    canvas.parallaxiaManager.play();
});


Hooks.on("canvasReady", async (canvas) => {
    canvas.parallaxiaManager.isPaused = game.paused;
    let num_targets = canvas.parallaxiaManager.gather_targets().length;
    paraldbg(`Found ${num_targets} targets in scene.`)

    // force a single update tick on scene start to set up tiles even if paused
    // is this the point where the first mipmap-removal updates may happen earliest?
    if (canvas.parallaxiaManager.isPaused) canvas.parallaxiaManager.refresh(0);
});


Hooks.on('ready', async () => {
    paraldbg('Ready!');
    if (game.user.isGM) {
        Hooks.on('renderTileHUD', (app, html, data) => {
            ParallaxiaIcons.addParallaxiaButton(app, html, data)
        });
    }
});
//
//
// Hooks.on('pauseGame', async (isPaused) => {
//     canvas.parallaxiaManager.isPaused = isPaused;
// });


