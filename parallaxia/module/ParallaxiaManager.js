import {paraldbg, parallog} from "./logging.js";
import {makeTiling} from "./ParallaxiaTile.js";

export class ParallaxiaManager {
    constructor() {
        this.layerKeys = ["foreground", "background"]
        this.targets = null;
        this.err = null;
        this.isPaused = false;

        // check if Haste module installed
        this.haste = function () {
        };
        if (typeof HasteSelf !== undefined && typeof HasteSelf === 'function') {
            parallog('Haste detected!')
            this.haste = HasteSelf;
        }
        return this;
    }

    tileFromId(tile_id) {
        let tile = null;
        for (const lk in this.layerKeys) {
            tile = canvas[lk].get(tile_id);
            if (tile) return tile;
        }
    }

    refresh(delta) {
        const t = Date.now(); // TODO: add synchronized Foundry Time!
        this.gather_targets();

        // canvas.scene.tiles has the documents
        // canvas.foreground/background etc. has the actual entity/thing
        // we need both - the document to push to the server, the instance for display updates

        // if we have targets to render, bump haste to prevent renderer going to sleep if Haste module installed
        if (this.targets.length) {
            this.haste();
        }

        // assuming this lookup is fast, not going to cache the result
        this.targets.forEach(tile => {
            if (!tile.hasOwnProperty("isParallaxia")) {
                if (!tile.conversion_attempted) {
                    this.makeParallaxia(tile);
                } else {
                    paraldbg("Failed tile repeated attempts");
                }
                return
            }

            if (tile.isParallaxia) {
                tile._advanceState(t, delta);
                if (tile._updating) {
                    paraldbg('Update in progress, not updating tile position')
                } else {
                    tile._applyState(tile.data.parallaxia.current);
                    if (!tile._refreshed) tile.refresh();
                }
            }
        })
    }

    remove_target = (tile_id) => {
        this.targets = this.targets.filter(tile => tile.id !== tile_id);
    }

    gather_targets = () => {
        this.targets = [];
        this.layerKeys.forEach(lk => {
            canvas[lk].placeables.forEach(tile => {
                // if (tile.document.getFlag('parallaxia', 'isTarget')) {
                if (tile.data.flags.parallaxia?.isTarget) {
                    this.targets.push(tile);
                }
            })
        });
        return this.targets;
    }

    async makeParallaxia(tile) {
        tile.conversion_attempted = true;
        if (tile.isParallaxia) return;
        parallog(`Turning base tile ${tile.id} into Parallaxia target`);

        await makeTiling(tile);
        tile.isParallaxia = true;
        parallog(`Upgrade of tile ${tile.id} complete!`);

        if (!tile.data.flags.parallaxia?.isTarget) {
            await tile.document.setFlag('parallaxia', 'isTarget', true);
        }
    }

    // add_target(tile_id) {
    //     if (tile_id.hasOwnProperty('_id')) tile_id = tile_id._id;
    //     if (this.targets.includes(tile_id)) return
    //     this.targets.push(tile_id);
    // }

    add_target(tile) {
        if (!this.targets.includes(tile)) this.targets.push(tile);
    }

    play() {
        parallog('Manager play loop start');
        canvas.app.ticker.add(this.refresh.bind(this));
    };

    clearAllTiles = () => {
        let d = new Dialog({
            title: "Clear all Tiles",
            content: "<p>Are you sure you want to reset all tiles in the scene to base tiles and redraw the scene?</p>",
            buttons: {
                one: {
                    icon: '<i class="fas fa-check"></i>',
                    label: "Yes",
                    callback: () => this._clearLayerTiles()
                },
                two: {
                    icon: '<i class="fas fa-times"></i>',
                    label: "Cancel",
                    // callback: () => console.log("Chose Two")
                }
            },
            default: "two",
            // close: () => console.log("This always is logged no matter which option is chosen")
        });
        d.render(true);
    }

    _clearLayerTiles = async () => {
        parallog('Removing Parallaxia states from all tiles on layer...')
        // alternatively I could step through targets only?
        for (const tile of canvas.activeLayer.placeables) {
            await tile.document.update({"flags.-=parallaxia": null});
            await tile.document.setFlag("core", "isTilingSprite", false);
        }

        await canvas.draw(); // what exactly does this do? Force a redraw?
    }
}
