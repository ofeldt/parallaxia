import {paraldbg, parallog} from "./logging.js";
import {blendModes, blendModesString} from "./util.js";

Hooks.on('renderParallaxiaTileConfig', (sheet, html) => {
    paraldbg('Render Hook for ParallaxiaTileConfig');
    // this is modeled after tidy-ui_game-settings.js by @sdenec
    // using jQuery instead of handlebar templates to populate the form

    let list = '.tab[data-tab="filters"] .filter-list';
    let active = html.find(list);

    // TODO: Looks like we stopped here mid-thought. Good job.
    let filters = sheet.object.data.parallaxia.current.filters;
    filters.forEach(f => {
        list
    })

});

export class ParallaxiaTileConfig extends TileConfig {
    constructor() {
        super(...arguments);
        this.interval = null;
        this._ptransformHelpSheet = new CustomFunctionHelp();
    }

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "parallaxia-tile-config",
            classes: ["sheet", "parallaxia-tile-sheet"],
            template: "modules/parallaxia/templates/parallaxia-tile-config.html",
            submitOnChange: true,
            height: Math.min(window.innerHeight - 170, 1100),
            tabs: [{navSelector: ".sheet-tabs", contentSelector: ".content", initial: "base"}],
            resizable: true
        });
    }

    /** @override */
    get title() {
        return `Parallaxia ${this.document.data.flags.parallaxia.name ?? this.document.id}`;
    }

    /* -------------------------------------------- */

    /** @override */
    getData(options) {
        paraldbg("SHEET GET DATA");
        const context = super.getData(options);

        this.interval = setInterval(this._update_current, 200, this);
        context.document = this.document;
        context.tileData = context.document.object.data;
        context.parallaxia = context.tileData.parallaxia;
        context.options = this.options;
        context.submitText = this.options.preview ? "Create" : "Save";
        context.options.submitOnClose = true;
        context.blendModes = blendModes;
        context.currentBlendMode = context.tileData.parallaxia.initial.blendMode;
        return context;
    }

    /* -------------------------------------------- */

    /** @override */
    async _updateObject(event, formData) {
        if (!game.user.isGM) throw "You do not have the ability to configure a Parallaxia object.";
        // formData["id"] = this.document.id;
        paraldbg('_updateObject', formData);
        if ( this.document.id ) {
            paraldbg("Updating document");
            return this.document.update(formData);
        }
        else return this.document.constructor.create(formData, {
            parent: this.document.parent,
            pack: this.document.pack
        });
    }

    /* -------------------------------------------- */

    /** @override */
    // when form input fields have changed, trigger an update as a preview
    // NOTE: We are here updating the CURRENT, not the INITIAL for the preview. THAT IS BAD UX.
    async _onChangeInput(event) {
        const fd = new FormDataExtended(this.form); //event.currentTarget
        paraldbg('onChangeInput', fd);
        for (let [form_key, form_value] of fd.entries()) {
            let field = this.document.data.parallaxia;
            let accessors = form_key.split('.');
            if (accessors[0] === 'initial') {
                // console.log('FD:', form_key, form_value, accessors);
                accessors.forEach((ks, ns) => {
                    if (!ns) {  // debug override, writing to current while previewing from the dialog
                        field = field['current'];
                    } else if (ns < accessors.length - 1) {
                        field = field[ks];
                    } else {
                        field[ks] = fd.dtypes[form_key] === "Number" ? parseFloat(form_value) : form_value;
                    }
                })
            }
        }
        this.document.object._ptransformSetup(this.form.ptransform.value); //event.currentTarget
        this.document.object.refresh();
    }

    /** Updates the TileConfig sheet, called regularly to keep responsive to live changes */
    _update_current(form) {
        if (!form.rendered) return;
        let tileDocument = form.document;
        let current = tileDocument.data.parallaxia.current;
        let previous = tileDocument.data.parallaxia.previous;

        form.form.curr_x.value = current.position.x.toFixed(1);
        form.form.curr_y.value = current.position.y.toFixed(1);

        // position change rate
        form.form.curr_dx.value = ((current.position.x - previous.position.x) / current.tDelta).toFixed(2);
        form.form.curr_dy.value = ((current.position.y - previous.position.y) / current.tDelta).toFixed(2);

        form.form.curr_width.value = current.width.toFixed(1);
        form.form.curr_height.value = current.height.toFixed(1);

        form.form.curr_rotation.value = current.rotation.z.toFixed(2);
        form.form.curr_tint_str.value = current.tint;
        form.form.curr_alpha.value = current.alpha.toFixed(1);

        // texture scale
        form.form.curr_scale_x.value = current.tiling.sx.toFixed(2);
        form.form.curr_scale_y.value = current.tiling.sy.toFixed(2);

        // texture scale change rate
        form.form.curr_speed_sx.value = ((current.tiling.sx - previous.tiling.sx) / current.tDelta).toFixed(2);
        form.form.curr_speed_sy.value = ((current.tiling.sy - previous.tiling.sy) / current.tDelta).toFixed(2);

        // texture offset
        form.form.curr_tiling_x.value = current.tiling.x.toFixed(2);
        form.form.curr_tiling_y.value = current.tiling.y.toFixed(2);

        // texture offset change rate
        form.form.curr_speed_x.value = ((current.tiling.x - previous.tiling.x) / current.tDelta).toFixed(2);
        form.form.curr_speed_y.value = ((current.tiling.y - previous.tiling.y) / current.tDelta).toFixed(2);
    }

    _showCustomFunctionHelp() {
        paraldbg('Render Custom Function Help Sheet');
        console.log(this);
        this._ptransformHelpSheet.render(true);
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers
    /* -------------------------------------------- */

    /**
     * Activate event listeners using the prepared sheet HTML
     * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
     */
    activateListeners(html) {
        const btnMacroHelp = html.find(".ptransformHelp");
        btnMacroHelp.on("click", this._showCustomFunctionHelp.bind(this));

        // Handle default listeners last so system listeners are triggered first
        super.activateListeners(html);
    }

    /** @override */
    async close(options) {
        await super.close(options);
        const layer = this.object.layer;
        layer.preview?.removeChildren();
        this.options.preview = false;
        clearInterval(this.interval);
    }
}

export class CustomFunctionHelp extends Application {
    constructor() {
        super(...arguments);
    }

    /* -------------------------------------------- */

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            template: "modules/parallaxia/templates/custom-function-help.html",
            width: 550,
            height: window.innerHeight - 100,
            title: "Parallaxia Help",
        });
    }
}